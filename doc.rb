require 'win32ole'

wd = WIN32OLE.new('Word.Application')
wd.visible = false
doc = wd.Documents.Open("#{Dir.pwd}/test.docx")
doc.CustomDocumentProperties.each do |c|
  c.Value = "3.0.2"
end
doc.Fields.Update
doc.Save
doc.ExportAsFixedFormat({
  "OutputFileName" => "#{Dir.pwd}/test.pdf",
  "ExportFormat" => 17,
  "IncludeDocProps" => false,
  "OpenAfterExport" => false
})
doc.Close
wd.Quit
